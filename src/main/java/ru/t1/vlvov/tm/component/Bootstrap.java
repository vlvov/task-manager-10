package ru.t1.vlvov.tm.component;

import ru.t1.vlvov.tm.api.*;
import ru.t1.vlvov.tm.constant.ArgumentConst;
import ru.t1.vlvov.tm.constant.TerminalConst;
import ru.t1.vlvov.tm.controller.CommandController;
import ru.t1.vlvov.tm.controller.ProjectController;
import ru.t1.vlvov.tm.controller.TaskController;
import ru.t1.vlvov.tm.repository.CommandRepository;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.service.CommandService;
import ru.t1.vlvov.tm.service.ProjectService;
import ru.t1.vlvov.tm.service.TaskService;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository= new ProjectRepository();

    private final IProjectService projectService= new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository= new TaskRepository();

    private final ITaskService taskService= new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void run(String[] args) {
        if (processArguments(args)) exit();
        processCommands();
    }

    private boolean processArguments(final String args[]) {
        if (args == null || args.length == 0) return false;
        final String argument = args[0];
        processArgument(argument);
        return true;
    }

    private void processArgument(final String argument) {
        switch (argument){
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    private void processCommands() {
        System.out.println("** WELCOME TO TASK-MANAGER**");
        String command;
        while(true) {
            System.out.println("ENTER COMMAND");
            command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }
        switch (command){
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
