package ru.t1.vlvov.tm.service;

import ru.t1.vlvov.tm.api.ICommandRepository;
import ru.t1.vlvov.tm.api.ICommandService;
import ru.t1.vlvov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
