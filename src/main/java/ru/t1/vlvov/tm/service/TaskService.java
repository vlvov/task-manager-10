package ru.t1.vlvov.tm.service;

import ru.t1.vlvov.tm.api.IProjectRepository;
import ru.t1.vlvov.tm.api.ITaskRepository;
import ru.t1.vlvov.tm.api.ITaskService;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService (final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if (name == "" || name.isEmpty()) return null;
        return add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == "" || name.isEmpty()) return null;
        if (description == "" || description.isEmpty()) return null;
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> showAll() {
        return taskRepository.showAll();
    }

}
