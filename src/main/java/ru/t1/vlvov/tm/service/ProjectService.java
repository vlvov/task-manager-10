package ru.t1.vlvov.tm.service;

import ru.t1.vlvov.tm.api.IProjectRepository;
import ru.t1.vlvov.tm.api.IProjectService;
import ru.t1.vlvov.tm.model.Project;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService (final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name) {
        if (name == "" || name.isEmpty()) return null;
        return add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == "" || name.isEmpty()) return null;
        if (description == "" || description.isEmpty()) return null;
        return add(new Project(name, description));
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> showAll() {
        return projectRepository.showAll();
    }

}
