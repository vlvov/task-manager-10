package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.ICommandRepository;
import ru.t1.vlvov.tm.constant.ArgumentConst;
import ru.t1.vlvov.tm.constant.TerminalConst;
import ru.t1.vlvov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(TerminalConst.ABOUT, ArgumentConst.ABOUT,"Show developer info.");

    private final static Command EXIT = new Command(TerminalConst.EXIT, null, "Close application.");

    private final static Command HELP = new Command(TerminalConst.HELP, ArgumentConst.HELP, "Show application commands.");

    private final static Command VERSION = new Command(TerminalConst.VERSION, ArgumentConst.VERSION, "Show application version.");

    private final static Command INFO = new Command(TerminalConst.INFO, ArgumentConst.INFO, "Show available memory.");

    private final static Command COMMANDS = new Command(TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show commands.");

    private final static Command ARGUMENTS = new Command(TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show arguments.");

    private final static Command PROJECT_CREATE = new Command(TerminalConst.PROJECT_CREATE, "", "Create new project.");

    private final static Command PROJECT_CLEAR = new Command(TerminalConst.PROJECT_CLEAR, "", "Remove all projects.");

    private final static Command PROJECT_LIST = new Command(TerminalConst.PROJECT_LIST, "", "Display all projects.");

    private final static Command TASK_CREATE = new Command(TerminalConst.TASK_CREATE, "", "Create new task.");

    private final static Command TASK_CLEAR = new Command(TerminalConst.TASK_CLEAR, "", "Remove all tasks.");

    private final static Command TASK_LIST = new Command(TerminalConst.TASK_LIST, "", "Display all tasks.");


    private final static Command[] TERMINAL_COMMANDS = new Command[] {
        ABOUT, EXIT, HELP, VERSION, INFO, COMMANDS, ARGUMENTS, PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, TASK_CREATE, TASK_CLEAR, TASK_LIST
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
