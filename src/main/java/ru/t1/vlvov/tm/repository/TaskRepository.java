package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.ITaskRepository;
import ru.t1.vlvov.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> showAll() {
        return tasks;
    }

}
