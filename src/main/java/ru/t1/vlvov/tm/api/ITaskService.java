package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Task;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    List<Task> showAll();
}
