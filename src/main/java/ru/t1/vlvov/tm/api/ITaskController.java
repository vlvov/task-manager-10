package ru.t1.vlvov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();
}
