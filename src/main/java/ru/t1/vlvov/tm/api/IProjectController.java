package ru.t1.vlvov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();
}
