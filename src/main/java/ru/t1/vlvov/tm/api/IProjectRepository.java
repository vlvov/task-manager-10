package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> showAll();

}
