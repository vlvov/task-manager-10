package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();

    List<Project> showAll();

}
