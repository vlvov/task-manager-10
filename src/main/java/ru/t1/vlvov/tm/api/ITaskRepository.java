package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> showAll();
}
